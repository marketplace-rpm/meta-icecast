%global app                     icecast

Name:                           meta-icecast
Version:                        1.0.0
Release:                        5%{?dist}
Summary:                        META-package for install and configure Icecast
License:                        GPLv3

Source10:                       %{app}.local.xml

Requires:                       icecast

%description
META-package for install and configure Icecast.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{_sysconfdir}/%{app}.local.xml


%files
%config %{_sysconfdir}/%{app}.local.xml


%changelog
* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-5
- UPD: Configs.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- UPD: SPEC-file.

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- UPD: "icecast.local.xml".

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- NEW: 1.0.0-2.

* Fri Feb 15 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
